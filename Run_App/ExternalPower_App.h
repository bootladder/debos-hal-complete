/*
 * ExternalPower_App.h
 *
 * Created: 7/3/2016 5:44:48 PM
 *  Author: ODI
 */ 


#ifndef EXTERNALPOWER_APP_H_
#define EXTERNALPOWER_APP_H_

#include <stdbool.h>

void ExternalPower_Init(void);
void ExternalPower_TaskHandler(void);

void checkExternalPower(void);
bool is_external_power(void);

#endif /* EXTERNALPOWER_APP_H_ */
