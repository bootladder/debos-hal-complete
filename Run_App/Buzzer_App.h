/*
 * Buzzer_App.h
 */ 

#ifndef LED_AND_BUZZER_APP_H_
#define LED_AND_BUZZER_APP_H_

#include <stdint.h>
#include <stdbool.h>

void beep(uint16_t time_on);
void beep_later(uint16_t time_on);
volatile bool is_beeping(void);
void Buzzer_Init(void);

#endif /* LED_AND_BUZZER_APP_H_ */
