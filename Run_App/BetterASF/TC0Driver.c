#include "TC0Driver.h"

#include "tc.h"
#include "tc_interrupt.h"
#include "clock.h"
#include <system_interrupt.h>

struct tc_config timer_config;
struct tc_module module_inst;

tmr_callback_t appcallback = 0;

void TC0Driver_SetCallback(tmr_callback_t cb)
{
	appcallback = cb;
}

static void tc_ovf_callback(struct tc_module *const module_instance)
{
(void)module_instance;
	//tmr_ovf_callback();
if(appcallback)appcallback();
}

/*! \brief  hw timer compare callback
 */
static void tc_cca_callback(struct tc_module *const module_instance)
{
(void)module_instance;
	//tmr_cca_callback();
if(appcallback)appcallback(); 
}

void TC0Driver_Init(void)
{
	//uint8_t timer_multiplier;
	tc_get_config_defaults(&timer_config);
		timer_config.clock_source = GCLK_GENERATOR_1;
		timer_config.clock_prescaler = TC_CLOCK_PRESCALER_DIV2;
		timer_config.run_in_standby = true;
	timer_config.enable_capture_on_channel[TC_COMPARE_CAPTURE_CHANNEL_0] = true;
	timer_config.wave_generation  = TC_WAVE_GENERATION_MATCH_FREQ;

	timer_config.counter_16_bit.compare_capture_channel[0] = 164;

	tc_init(&module_inst, TC0, &timer_config);
	tc_register_callback(&module_inst, tc_ovf_callback,
			TC_CALLBACK_OVERFLOW);
	tc_register_callback(&module_inst, tc_cca_callback,
			TC_CALLBACK_CC_CHANNEL0);
	tc_enable_callback(&module_inst, TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&module_inst, TC_CALLBACK_CC_CHANNEL0);

	tc_enable(&module_inst);
	/* calculate how faster the timer with current clk freq compared to
	 *timer with 1Mhz */
	//timer_multiplier = system_gclk_gen_get_hz(0) / 1000000;
	//return timer_multiplier;
}


void TC0Driver_EnableInterrupts(void)
{
	NVIC_EnableIRQ(TC0_IRQn);
}
void TC0Driver_DisableInterrupts(void)
{
	NVIC_DisableIRQ(TC0_IRQn);
}
