/*
 * Battery_App.h
 *
 * Created: 7/3/2016 5:17:21 PM
 *  Author: ODI
 */ 


#ifndef BATTERY_APP_H_
#define BATTERY_APP_H_

#include <stdint.h>
#include <stdbool.h>

void ADC_Init(void);
void Battery_TaskHandler(void);

void read_battery_level(void);
uint8_t get_battery_level(void);
bool is_battery_fully_charged(void);
bool is_battery_alarm(void);
bool is_critical_battery_alarm(void);

void Battery_SetBatteryMessageSent(void);
void Battery_SetCriticalBatteryMessageSent(void);
void Battery_ClearAllFlags(void);
void Battery_BeginBatteryReadProcess(void);

void Battery_SetBatteryChargerFromVariable(void);
void Battery_Charger_On(void);
void Battery_Charger_Off(void);

#endif /* BATTERY_APP_H_ */
