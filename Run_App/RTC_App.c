/*
 * RTC_App.c
 *
 * Created: 8/18/2015 5:57:07 PM
 *  Author: ODI
 */

#include "asf.h"
#include "RTC_App.h"
#include "Variables.h"
#include "debug_helper.h"

static struct rtc_module rtc_instance;

#define RTC_PERIOD_SECONDS 14  //actually 2 seconds.  make it 15 later
#define TEST_MODE_INTERVALS 8 //15 seconds * 8 = 120 seconds


uint8_t My_SensorTimeSlot = 0;       //this one goes to 1 hour
bool    isSensorHeartbeatTime = false;
uint8_t My_ValveTimeSlot = 0;		//this one goes to 5 minutes
bool	isValveHeartbeatTime = false;
uint8_t My_BatteryTimeSlot = 0;
bool	isBatteryReadTime = true;  //initialize to true to start read at boot
bool	isBlinkTime = false;

bool criticalHeartbeatMessageSendFlag = false;
bool normalHeartbeatMessageSendFlag = false;
uint8_t normalMessageSendCounter  = 0;
uint8_t criticalMessageSendCounter  = 0;

static volatile uint32_t absolute_timepos = 1;

static volatile bool test_mode_active = false;
static volatile uint8_t test_mode_counter = 0;
static volatile bool test_mode_timeout = false;

static void rtc_overflow_callback(void);

#include "System.h"
#include "AppSystemCheckpoint.h"
#include <string.h>

static void rtc_overflow_callback(void)
{
memcpy(checkpointmessagebuffer,"INSSUPERAP ISR!!\n\0",18);
checkpoint2:
    (mycheckpointpointer)((uint32_t)(uintptr_t)&&checkpoint2,0x8|0x2,checkpointmessagebuffer);

//	//Keep track of time
	My_SensorTimeSlot++;
	My_ValveTimeSlot++;
	//My_BatteryTimeSlot++;
	absolute_timepos++;

  isBatteryReadTime = true;

	isBlinkTime = true;

	//Track sensor heartbeat period  every hour
	if( My_SensorTimeSlot >= TIME_SLOTS_FOR_1_HOUR )
	{
		isSensorHeartbeatTime = true;
		My_SensorTimeSlot = 0;

		normalMessageSendCounter++;
//		if( normalMessageSendCounter >= get_normalHeartbeatMessageInterval()  )
//		{
//			normalMessageSendCounter = 0;
//			normalHeartbeatMessageSendFlag = true;
//		}
	}

	if( My_ValveTimeSlot >= TIME_SLOTS_FOR_5_MINS ) //
	{
		isValveHeartbeatTime = true;
		My_ValveTimeSlot = 0;

		criticalMessageSendCounter += 1;
	//	if( criticalMessageSendCounter >= get_criticalHeartbeatMessageInterval()  )
	//	{
	//		criticalMessageSendCounter = 0;
	//		criticalHeartbeatMessageSendFlag = true;
	//	}

	}

  if( test_mode_active )
  {
    if( ++test_mode_counter >= TEST_MODE_INTERVALS )
    {
      test_mode_timeout = true;
    }
  }
	//if( My_BatteryTimeSlot >= TIME_SLOTS_FOR_5_MINS)
	//{
	//	My_BatteryTimeSlot = 0;
	//}


}

bool is_SensorHeartbeatTime(void){
	return isSensorHeartbeatTime;
}
void clear_SensorHeartbeatTime(void){
	isSensorHeartbeatTime = false;
}
uint8_t get_SensorTimeSlot(void){
	return My_SensorTimeSlot;
}

bool is_ValveHeartbeatTime(void){
	return isValveHeartbeatTime;
}
void clear_ValveHeartbeatTime(void){
	isValveHeartbeatTime = false;
}
void kick_ValveHeartbeatTimeSlot(void)
{
	My_ValveTimeSlot = TIME_SLOTS_FOR_5_MINS - 1;
}
uint8_t get_ValveTimeSlot(void){
	return My_ValveTimeSlot;
}


void RTC_NormalHeartbeatMessageSendFlag_Clear(void)
{
	normalHeartbeatMessageSendFlag = false;
}
void RTC_CriticalHeartbeatMessageSendFlag_Clear(void)
{
	criticalHeartbeatMessageSendFlag = false;
}
bool RTC_NormalHeartbeatMessageSendFlag_IsSet(void)
{
		return normalHeartbeatMessageSendFlag;
}
bool RTC_CriticalHeartbeatMessageSendFlag_IsSet(void)
{
		return criticalHeartbeatMessageSendFlag;
}

uint32_t RTC_GetAbsoluteTimepos(void)
{
	return absolute_timepos;
}

void force_set_sensor_heartbeat(void){
	isSensorHeartbeatTime = true;
}

bool RTC_IsBatteryReadTime(void){
	return isBatteryReadTime;
}

void clear_battery_read_time(void){
	isBatteryReadTime = false;
}

bool RTC_IsBlinkTime(void){
	return isBlinkTime;
}
void RTC_ClearBlinkTime(void){
	isBlinkTime = false;
}


void RTC_SetTestModeActive(void)
{
	test_mode_active = true;
}

bool RTC_IsTestModeActive(void)
{
	return test_mode_active;
}

bool RTC_IsTestModeTimeout(void)
{
	return test_mode_timeout;
}

void RTC_ClearTestMode(void)
{
	test_mode_timeout = false;
	test_mode_active = false;
	test_mode_counter = 0;
}

static void configure_rtc_count(void)
{
	//! [init_conf]
	struct rtc_count_config config_rtc_count;
	rtc_count_get_config_defaults(&config_rtc_count);

	//RTC uses GCLK2 = 32kHz/32 = 1Khz
	config_rtc_count.prescaler           = RTC_COUNT_PRESCALER_DIV_1024;//1HzRTC_COUNT_PRESCALER_DIV_1;//
	config_rtc_count.mode                = RTC_COUNT_MODE_16BIT;
	#ifdef FEATURE_RTC_CONTINUOUSLY_UPDATED
	config_rtc_count.continuously_update = true;
	#endif

	rtc_count_init(&rtc_instance, RTC, &config_rtc_count);
}

static void configure_rtc_callbacks(void)
{
	rtc_count_register_callback(
		&rtc_instance, rtc_overflow_callback, RTC_COUNT_CALLBACK_OVERFLOW);

	rtc_count_enable_callback(&rtc_instance, RTC_COUNT_CALLBACK_OVERFLOW);
}


void RTC_Init(void)
{
	configure_rtc_count();

	configure_rtc_callbacks();

	rtc_count_set_period(&rtc_instance, RTC_PERIOD_SECONDS);
	rtc_count_enable(&rtc_instance);
system_interrupt_enable(RTC_IRQn);

}
