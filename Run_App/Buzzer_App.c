
/*
 * LED_App.c
 *
 * Created: 8/7/2015 5:32:21 PM
 *  Author: ODI
 */ 
#include "Buzzer_App.h"
#include "Variables.h"

#include <stdint.h>
#include <stdbool.h>
#include <samd20_xplained_pro.h>
#include "gclk.h"
#include "port.h"
#include "tc.h"
#include "tc_interrupt.h"
#include "sysTimer.h"

static void configure_tc4(void);
static void tc_callback_to_buzzer_oneBeep(
								struct tc_module *const module_inst);

/************************************************************************/
/*  BUZZER                                                              */
/************************************************************************/


#define PWM_MODULE      EXT2_PWM_MODULE
#define PWM_OUT_PIN     EXT2_PWM_0_PIN
#define PWM_OUT_MUX     EXT2_PWM_0_MUX

#define CONF_TC_MODULE_CTRL TC6

struct tc_module tc_instance3;
struct tc_module tc_instance4;

static uint16_t beep_count = 0;


//PA22 controls buzzer
//Use TC4 to generate PWM 2Khz
static void configure_tc4(void)
{
	struct tc_config config_tc;
	tc_get_config_defaults(&config_tc);
	config_tc.counter_size = TC_COUNTER_SIZE_8BIT;
	config_tc.clock_source = GCLK_GENERATOR_4; //32kHz
	config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV1; //32kHz
	config_tc.counter_8_bit.period = 16;

	config_tc.pwm_channel[0].enabled = true;
	config_tc.pwm_channel[0].pin_out = PWM_OUT_PIN;
	config_tc.pwm_channel[0].pin_mux = PWM_OUT_MUX;
	tc_init(&tc_instance4, PWM_MODULE, &config_tc);
	//	tc_enable(&tc_instance4);

}

static volatile bool isBeeping = false;
volatile bool is_beeping(void)
{
  return isBeeping;
}
static uint16_t laterBeepDuration = 0;


static void tc_callback_to_buzzer_oneBeep(
	struct tc_module *const module_inst)
{
	tc_disable(&tc_instance4);
  isBeeping = false;
}
//TC3 turn on/off PWM at pin PA22
//time_on qand time_off << 8*256
void beep(uint16_t time_on)
{
  cpu_irq_disable();

  if( isBeeping )
  {
    beep_later(time_on);

    cpu_irq_enable();
    return;
  }

//	if( get_enableBuzzer() == false )
//		return;
		
	//if (beep_count == 0)
	{
		beep_count++;
		struct tc_config config_tc;
		tc_get_config_defaults(&config_tc);

		config_tc.counter_size = TC_COUNTER_SIZE_8BIT;
		config_tc.clock_source = GCLK_GENERATOR_2; //1kHz
		config_tc.clock_prescaler = TC_CLOCK_PRESCALER_DIV1; //1kHz
		config_tc.counter_8_bit.period = time_on *1 ;
		config_tc.oneshot = true;
		tc_disable(&tc_instance3);

		tc_init(&tc_instance3, CONF_TC_MODULE_CTRL, &config_tc);
		tc_enable(&tc_instance3);

system_interrupt_enable(TC6_IRQn);
		
		tc_register_callback(&tc_instance3, tc_callback_to_buzzer_oneBeep,
			TC_CALLBACK_OVERFLOW);
		tc_enable_callback(&tc_instance3, TC_CALLBACK_OVERFLOW);
		
		tc_enable(&tc_instance4);
	}
    isBeeping = true; 
    cpu_irq_enable();
}
static void beepLaterHandler(SYS_Timer_t * timer)
{
  beep(laterBeepDuration);
}

void beep_later(uint16_t time_on)
{
  laterBeepDuration = time_on;
  static SYS_Timer_t timer;
  timer.interval = 10;
  timer.mode = SYS_TIMER_INTERVAL_MODE;
  timer.handler = beepLaterHandler;
  SYS_TimerStart(&timer);
}


void Buzzer_Init(void)
{
	configure_tc4();
} 
