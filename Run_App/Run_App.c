#include "Run_App.h"
#include "AppSystemCheckpoint.h"

#include "sysTimer.h"
#include "LED_App.h"
#include "Buzzer_App.h"
#include "RTC_App.h"
#include "my_memcpy.h"
#include "system.h"
#include "phy.h"
#include "Battery_App.h"
#include "Button_App.h"
#include "ExternalPower_App.h"

#define CHECKPOINT_FLAG_LED 0x10
#define CHECKPOINT_FLAG_PRINTMSG 0x8
#define CHECKPOINT_FLAG_PRINTADDR 0x2

static void _blink_handler(struct SYS_Timer_t *timer)
{
    AppSystemWriteCheckpointBuffer((uint8_t*)"ARRRRRR   \n", 20);
    AppSystemCheckpoint(0x30303A33, 0x2|0x8|CHECKPOINT_FLAG_LED);
LED_Blink_B(50);
//beep(10);
(void)timer;
}

void Init_App(void)
{
	static uint8_t init_string[20];
	my_memcpy(init_string, (uint8_t*)"HALComp(X.X.X)      \n", 20);
	init_string[8]  = '0' + MAJOR_VERSION_NUMBER;
	init_string[10] = '0' + MINOR_VERSION_NUMBER;
	init_string[12] = '0' + BUILD_VERSION_NUMBER;
    AppSystemWriteCheckpointBuffer(init_string, 20);
    AppSystemCheckpoint(0x30303A33, 0x2|0x8);

	system_init();
    SYS_TimerInit();
    Buzzer_Init();
	LED_Init();
	RTC_Init();
	ADC_Init();
	Button_Init();
	ExternalPower_Init();

	PHY_Init();
	

static SYS_Timer_t blinktimer;
blinktimer.interval = 1000;
blinktimer.mode = SYS_TIMER_PERIODIC_MODE;
blinktimer.handler = _blink_handler;
SYS_TimerStart(&blinktimer);
}


void Run_App(void)
{
    SYS_TimerTaskHandler();
    PHY_TaskHandler();
    Button_TaskHandler();
	Battery_TaskHandler();

	if(RTC_IsBlinkTime())
	{
		RTC_ClearBlinkTime();
		LED_Blink_R(50);
	}
    return;
}
