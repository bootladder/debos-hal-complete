/*
 * Button_App.h
 *
 * Created: 1/27/2016 12:11:50 PM
 *  Author: ODI
 */ 


#ifndef BUTTON_APP_H_
#define BUTTON_APP_H_

#include <compiler.h>

void clear_button2_presses(void);


bool is_button2_pressed_and_released(void);
bool is_button2_pressed_3_seconds(void);
bool is_button2_pressed_10_seconds(void);
bool is_button2_pressed_20_seconds(void);
bool is_button2_waiting_for_release(void);

void Button_TaskHandler(void);
void Button_Init(void);

#endif /* BUTTON_APP_H_ */
