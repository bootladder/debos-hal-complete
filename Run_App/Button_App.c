/*
 * Button_App.c
 *
 * Created: 10/16/2015 6:09:33 PM
 *  Author: ODI
 */ 
#include "Button_App.h"

#include <compiler.h>
#include "port.h"
#include "sysTimer.h"
#include "LED_App.h"
#include "Buzzer_App.h"
#include "extint.h"
#include "extint_callback.h"
#include "samd20_xplained_pro.h"
#include "Variables.h"

static void configure_button_release_callback(void);
static void configure_button_press_callback(void);
static void disable_button_release_callback(void);
static void disable_button_press_callback(void);
static void configure_button_release(void);
static void configure_button_press(void);

#define BUTTON2_PIN PIN_PA15

enum {WAITING,DEBOUNCING,PRESSED,DEBOUNCING2,RELEASED};

static bool button2_level;  //the gpio input
static bool button2_pressed, button2_released;  //state flags

static bool button2_debounced;
static SYS_Timer_t Button2DebounceTimer;
static SYS_Timer_t Button2PressReleaseTimer, 
				   Button2Press10SecTimer  , Button2Press11SecTimer,
				   Button2Press20SecTimer  , Button2Press21SecTimer;
			
static bool button2_pressed_and_released, 
		button2_pressed_10, button2_pressed_20;

//kluedge shadow ofthe static state variable
static uint8_t button2_state  = 0;

bool is_button2_pressed_and_released(void)
{
	return button2_pressed_and_released && button2_released && !button2_pressed;
}
bool is_button2_pressed_10_seconds(void)
{
	return button2_pressed_10 && button2_released && !button2_pressed;
}
bool is_button2_pressed_20_seconds(void)
{
	return button2_pressed_20 && button2_released && !button2_pressed;
}
bool is_button2_waiting_for_release(void)
{
  return button2_level == 1;
}

void clear_button2_presses(void)
{
	button2_pressed_and_released = false;
	button2_pressed_10 = false;
	button2_pressed_20 = false;
}

/************************************************************************/
// BUTTON 2:  THE MIDDLE BUTTON
/************************************************************************/

static void Button2(bool level)
{
	static uint8_t state = WAITING;
  button2_state   = state;
	switch(state)
	{
		case WAITING:
			if( level )  //wait for a press (high)
			{
				state = DEBOUNCING;
				button2_debounced = false;
				SYS_TimerStart(&Button2DebounceTimer);
			}
			break;
			
		case DEBOUNCING:
			if( button2_debounced )	//wait for debounce timer
			{
				if( level )	{		//If still pressed (high)
					state = PRESSED;
					
          //LED_Blink_B( get_activeLEDDuration() );
          beep(50);

					button2_pressed = true;
					button2_released = false;
					
					button2_pressed_and_released = true;
					button2_pressed_10 = false;
					button2_pressed_20 = false;
				
					SYS_TimerStart(&Button2PressReleaseTimer);
					SYS_TimerStart(&Button2Press10SecTimer);
					SYS_TimerStart(&Button2Press11SecTimer);
					SYS_TimerStart(&Button2Press20SecTimer);
					SYS_TimerStart(&Button2Press21SecTimer);
					
				}
				else
					state = WAITING;
			}
			break;
		
		case PRESSED:
			if( ! level ){  //wait for release (low)
				state = DEBOUNCING2;
				button2_debounced = false;
				SYS_TimerStart(&Button2DebounceTimer);
			}
			break;
			
		case DEBOUNCING2:
			if( button2_debounced )  //wait for debounce timer
			{
				if( ! level ){  //if still low
				
					button2_pressed = false;
					button2_released = true;
					
					SYS_TimerStop(&Button2PressReleaseTimer);
					SYS_TimerStop(&Button2Press10SecTimer);
					SYS_TimerStop(&Button2Press11SecTimer);				
					SYS_TimerStop(&Button2Press20SecTimer);
					SYS_TimerStop(&Button2Press21SecTimer);
					state = WAITING;
				}
				else
					state = DEBOUNCING2;
			}
			break;
	}
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////


void Button_TaskHandler(void)
{
	button2_level = ! port_pin_get_input_level(BUTTON2_PIN);
	Button2(button2_level);
}

static void config_buttons(void)
{

	struct port_config config_port_pin;
	port_get_config_defaults(&config_port_pin);
	
	config_port_pin.direction = PORT_PIN_DIR_INPUT;
	config_port_pin.input_pull = PORT_PIN_PULL_UP;
	port_pin_set_config(BUTTON2_PIN, &config_port_pin);

//  struct extint_chan_conf config_extint_chan;
//  extint_chan_get_config_defaults(&config_extint_chan);
//
//  config_extint_chan.gpio_pin           = BUTTON_0_EIC_PIN;
//  config_extint_chan.gpio_pin_mux       = BUTTON_0_EIC_MUX;
//  config_extint_chan.gpio_pin_pull      = EXTINT_PULL_UP;
//  config_extint_chan.detection_criteria = EXTINT_DETECT_LOW;//EXTINT_DETECT_BOTH;
//
//  extint_chan_set_config(BUTTON_0_EIC_LINE, &config_extint_chan);

}

/************************************************************************/
/************************************************************************/
/************************************************************************/
static void Button2DebounceHandler(SYS_Timer_t * timer)
{
	button2_debounced = true;
}
static void Button2PressHandler(SYS_Timer_t * timer)
{
	button2_pressed_and_released = false;
}
static void Button2Press10SecHandler(SYS_Timer_t * timer)
{
	button2_pressed_10 = true;
  //LED_Blink_B( get_activeLEDDuration() );
	beep(50);
}
static void Button2Press11SecHandler(SYS_Timer_t * timer)
{
	button2_pressed_10 = false;
}
static void Button2Press20SecHandler(SYS_Timer_t * timer)
{
	button2_pressed_20 = true;
  //LED_Blink_B( get_activeLEDDuration() );
	beep(50);
}
static void Button2Press21SecHandler(SYS_Timer_t * timer)
{
	button2_pressed_20 = false;
}

void Button_Init(void)
{
	//CONFIGURE BUTTON PRESS 
	config_buttons();
//  configure_button_release_callback();
	
	Button2DebounceTimer.handler = Button2DebounceHandler;
	Button2DebounceTimer.interval = 10;
	Button2DebounceTimer.mode = SYS_TIMER_INTERVAL_MODE;

	Button2PressReleaseTimer.handler = Button2PressHandler;
	Button2PressReleaseTimer.interval = 500;
	Button2PressReleaseTimer.mode = SYS_TIMER_INTERVAL_MODE;

	Button2Press10SecTimer.handler = Button2Press10SecHandler;
	Button2Press10SecTimer.interval = 10000;
	Button2Press10SecTimer.mode = SYS_TIMER_INTERVAL_MODE;
	
	Button2Press11SecTimer.handler = Button2Press11SecHandler;
	Button2Press11SecTimer.interval = 11000;
	Button2Press11SecTimer.mode = SYS_TIMER_INTERVAL_MODE;

	Button2Press20SecTimer.handler = Button2Press20SecHandler;
	Button2Press20SecTimer.interval = 20000;
	Button2Press20SecTimer.mode = SYS_TIMER_INTERVAL_MODE;
	
	Button2Press21SecTimer.handler = Button2Press21SecHandler;
	Button2Press21SecTimer.interval = 21000;
	Button2Press21SecTimer.mode = SYS_TIMER_INTERVAL_MODE;
	
}


//HIGH LEVEL INTERRUPT
static void button_released_callback(void)
{
  uint8_t flag = cpu_irq_save();
  //cpu_irq_disable();
  
		disable_button_release_callback();
		configure_button_press();
		configure_button_press_callback();
	//this line is required, otherwise another interrupt happens
	extint_chan_clear_detected(BUTTON_0_EIC_LINE);

  //cpu_irq_enable();
  cpu_irq_restore(flag);
}


//LOW LEVEL INTERRUPT
static void button_pressed_callback(void)
{
  //cpu_irq_disable();
  uint8_t flag = cpu_irq_save();

		//Enable HIGH LEVEL Interrupt (Button Release) and assign new callback
		disable_button_press_callback();
		configure_button_release();
		configure_button_release_callback();
	//
	//this line is required, otherwise another interrupt happens
	extint_chan_clear_detected(BUTTON_0_EIC_LINE);  
  //cpu_irq_enable();
  cpu_irq_restore(flag);
}

static void configure_button_release_callback(void)
{
  extint_register_callback(button_released_callback,
    BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);

  extint_chan_enable_callback(BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);
}

static void configure_button_press_callback(void)
{
  extint_register_callback(button_pressed_callback,
    BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);

  extint_chan_enable_callback(BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);
}
static void disable_button_release_callback(void)
{
  extint_unregister_callback(button_released_callback,
    BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);

  extint_chan_disable_callback(BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);
}

static void disable_button_press_callback(void)
{
  extint_unregister_callback(button_pressed_callback,
    BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);

  extint_chan_disable_callback(BUTTON_0_EIC_LINE,
    EXTINT_CALLBACK_TYPE_DETECT);
}



static void configure_button_press(void)
{
  struct extint_chan_conf config_extint_chan;
  extint_chan_get_config_defaults(&config_extint_chan);

  config_extint_chan.gpio_pin           = BUTTON_0_EIC_PIN;
  config_extint_chan.gpio_pin_mux       = BUTTON_0_EIC_MUX;
  config_extint_chan.gpio_pin_pull      = EXTINT_PULL_UP;
  config_extint_chan.detection_criteria = EXTINT_DETECT_LOW;//EXTINT_DETECT_BOTH;

  extint_chan_set_config(BUTTON_0_EIC_LINE, &config_extint_chan);
}
static void configure_button_release(void)
{
  struct extint_chan_conf config_extint_chan;
  extint_chan_get_config_defaults(&config_extint_chan);

  config_extint_chan.gpio_pin           = BUTTON_0_EIC_PIN;
  config_extint_chan.gpio_pin_mux       = BUTTON_0_EIC_MUX;
  config_extint_chan.gpio_pin_pull      = EXTINT_PULL_UP;
  config_extint_chan.detection_criteria = EXTINT_DETECT_HIGH;//EXTINT_DETECT_BOTH;

  extint_chan_set_config(BUTTON_0_EIC_LINE, &config_extint_chan);
}

             
