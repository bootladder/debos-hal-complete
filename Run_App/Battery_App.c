/*
 * Battery_App.c
 *
 * Created: 5/15/2016 8:05:03 PM
 *  Author: ODI
 */ 
#include "Battery_App.h"

#include "adc.h"
#include "adc_callback.h"
#include "port.h"
#include "sysTimer.h"
#include "nwk.h"
//#include "Network_Info.h"
//#include "Alarm_Messages.h"
//#include "CommandFromBasestation_App.h"
#include "RTC_App.h"
#include "Variables.h"
//#include "LED_and_Buzzer_App.h"
//#include "Delay_App.h"
/*************************************/
#define BATT_CHK_PIN		PIN_PA02
#define BAT_CHK_EN_PIN		PIN_PB23
#define CONF_CHK_EN_PIN		PIN_PB22
#define BATT_CHARGE_PIN PIN_PA21
/*************************************/
static void configure_battery_callback(void);
static void battery_complete_callback(const struct adc_module *const module);
static void ADC_enable(void);
static void ADC_disable(void);
static void DischargeTimerHandler(SYS_Timer_t * timer);
static void LoadSwitches_Init(void);
static void Battery_Charger_Init(void);

/***************************************/
#define ADC_SAMPLES 8 //128
static uint16_t adc_buffer[ADC_SAMPLES];
static struct adc_module adc_instance;
/***************************************/
static struct port_config config_port_pin;
/***************************************/
static uint8_t battery_level = 0xFF;
static volatile bool waiting_for_battery_read = false;

static bool battery_message_sent = false;
static bool critical_battery_message_sent = false;
static bool Battery_Alarm = false;
static bool Critical_Battery_Alarm = false;


/*=======================================================
* | BATTERY LEVEL CHECK
* =======================================================*/
//
uint8_t get_battery_level(void)
{
	return battery_level;
}

static void battery_complete_callback(const struct adc_module *const module)
{
	ADC_disable();
	//Disable battery sense switch
	port_pin_set_output_level(BAT_CHK_EN_PIN, 0);
	waiting_for_battery_read = false;
	//compute the average
	uint32_t avg = 0;
	for(uint8_t i=0;i<ADC_SAMPLES;i++)
	{
		avg += adc_buffer[i];
	}
	
	battery_level = (uint8_t)(avg>>3);
	(void)module;
}

void read_battery_level(void)
{
	//Enable battery sense switch
	port_pin_set_output_level(BAT_CHK_EN_PIN, 1);

	ADC_enable();
	//configure_battery_callback();
	adc_set_positive_input(&adc_instance, ADC_POSITIVE_INPUT_PIN0);
	
	adc_read_buffer_job(&adc_instance, adc_buffer, ADC_SAMPLES);
	waiting_for_battery_read = true;
}

bool is_battery_fully_charged(void)
{
//	//if( waiting_for_battery_read )
//	//	return false;
//	
//	if ( battery_level >= get_fullyChargedBatteryLevel() )
//		return true;
//	else
		return false;
}
bool is_battery_alarm(void)
{
////	if( waiting_for_battery_read )
////		return false;
//	
//	if (battery_level < get_low_battery_setpoint())
//		return true;
//	else
		return false;
}
bool is_critical_battery_alarm(void)
{
////	if( waiting_for_battery_read )
////	return false;
//	
//	if (battery_level < get_criticalBatteryLevel())
//		return true;
//	else
		return false;
}


//To clear the flags
void Battery_SetBatteryMessageSent(void)
{
	battery_message_sent = true;
}
void Battery_SetCriticalBatteryMessageSent(void)
{
	critical_battery_message_sent= true;
}

void Battery_ClearAllFlags(void)
{
	battery_message_sent = false;
	critical_battery_message_sent = false;
	Battery_Alarm = false;
	Critical_Battery_Alarm = false;
}

void Battery_SetBatteryChargerFromVariable(void)
{
//  if( get_batteryChargerEnable() )
//    Battery_Charger_On();
//  else
//    Battery_Charger_Off();
}

void Battery_Charger_Off(void)
{
  port_pin_set_output_level(BATT_CHARGE_PIN,1); //high to off 
}
void Battery_Charger_On(void)
{
  port_pin_set_output_level(BATT_CHARGE_PIN,0); //low to on 
}


/************************************************************************/
// BATTERY READ PROCESS
/************************************************************************/
// To read the battery and also detect if the battery is disconnected,
// the battery must be connected to the sense circuitry for some time
// before the voltage is read.  this allows voltage to drop to zero
// if the battery is disconnected.

static SYS_Timer_t DischargeTimer;
static uint16_t    DischargeInterval = 200; //200 ms

static void DischargeTimerHandler(SYS_Timer_t * timer)
{
	read_battery_level();
}

void Battery_BeginBatteryReadProcess(void)
{
	//Enable battery sense switch
	port_pin_set_output_level(BAT_CHK_EN_PIN, 1);
	
	//Start Timer
	DischargeTimer.mode = SYS_TIMER_INTERVAL_MODE;
	DischargeTimer.interval = DischargeInterval;
	DischargeTimer.handler = DischargeTimerHandler;
	SYS_TimerStart(&DischargeTimer);
}


/************************************************************************/

/************************************************************************/
// BATTERY TASKHANDLER
/************************************************************************/
// Check Battery level every RTC interrupt.
// If Battery Alarm, Send message every 5 minutes, up to 12 times
/************************************************************************/
//static SYS_Timer_t batteryAlarmTimer;			//5 minute timer for sending messages
//static bool batteryAlarmTimerRunning;
////to be reconnected
//
//static void batteryAlarmTimerHandler(SYS_Timer_t * timer)
//{
//	batteryAlarmTimerRunning = false;
//}
//
//static void start_battery_alarm_timer(void)
//{
//	batteryAlarmTimer.interval = 300000;
//	batteryAlarmTimer.mode = SYS_TIMER_INTERVAL_MODE;
//	batteryAlarmTimer.handler = batteryAlarmTimerHandler;
//	SYS_TimerStart(&batteryAlarmTimer);
//	batteryAlarmTimerRunning = true;
//}
//
//static SYS_Timer_t criticalBatteryAlarmTimer;
//static bool criticalBatteryAlarmTimerRunning;
//
//static void criticalBatteryAlarmTimerHandler(SYS_Timer_t * timer)
//{
//	criticalBatteryAlarmTimerRunning = false;
//}
//
//static void start_critical_battery_alarm_timer(void)
//{
//	criticalBatteryAlarmTimer.interval = 300000;
//	criticalBatteryAlarmTimer.mode = SYS_TIMER_INTERVAL_MODE;
//	criticalBatteryAlarmTimer.handler = criticalBatteryAlarmTimerHandler;
//	SYS_TimerStart(&criticalBatteryAlarmTimer);
//	criticalBatteryAlarmTimerRunning = true;
//}
/************************************************************************/

static bool low_battery_eventflag = false;
static bool critical_battery_eventflag = false;
static bool fullycharged_battery_eventflag = true;

void Battery_TaskHandler(void)
{
	if( RTC_IsBatteryReadTime() )
	{
		clear_battery_read_time();
		Battery_BeginBatteryReadProcess();
	}

  if( is_battery_fully_charged() )
  {
    if( false == fullycharged_battery_eventflag )
    {
      //AlarmMessages_Queue_FullyChargedBatteryMessage();	
      fullycharged_battery_eventflag = true;

//      LED_Blink_R(5);Delay_Millis(100);
      critical_battery_eventflag = false;
      low_battery_eventflag = false;
    }
  }
	
	if( is_battery_alarm() )
	{
    if( false == low_battery_eventflag )
    {
      low_battery_eventflag = true;

 //     LED_Blink_R(5);Delay_Millis(100);
      //AlarmMessages_Queue_LowBatteryMessage();	
      fullycharged_battery_eventflag = false;
    }
	}

	if( is_critical_battery_alarm() )  //low voltage
	{
    if( false == critical_battery_eventflag )
    {
      critical_battery_eventflag = true;

      //AlarmMessages_Queue_CriticalBatteryMessage();	
      fullycharged_battery_eventflag = false;
    }
	}


}
/************************************************************************/
void  Battery_Charger_Init(void)
{
	port_get_config_defaults(&config_port_pin);
	
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(BATT_CHARGE_PIN, &config_port_pin);
	
	port_pin_set_output_level(BATT_CHARGE_PIN, 0);
}

/************************************************************************/
/*=======================================================
* | LOAD SWITCHES INIT
* =======================================================*/
static void LoadSwitches_Init(void)
{
	port_get_config_defaults(&config_port_pin);
	
	config_port_pin.direction = PORT_PIN_DIR_INPUT;
	port_pin_set_config(BAT_CHK_EN_PIN, &config_port_pin);
	port_pin_set_config(CONF_CHK_EN_PIN, &config_port_pin);
	
	port_pin_set_output_level(CONF_CHK_EN_PIN, 0);
	port_pin_set_output_level(BAT_CHK_EN_PIN, 0);
}

/*=======================================================
* | ADC INIT
* =======================================================*/

static void ADC_disable(void)
{
	system_voltage_reference_disable(SYSTEM_VOLTAGE_REFERENCE_BANDGAP);
	adc_disable(&adc_instance);
}
static void ADC_enable(void)
{
	system_voltage_reference_enable(SYSTEM_VOLTAGE_REFERENCE_BANDGAP);
	adc_enable(&adc_instance);
}

static void configure_battery_callback(void)
{
	adc_register_callback(&adc_instance,
	battery_complete_callback, ADC_CALLBACK_READ_BUFFER);
	adc_enable_callback(&adc_instance, ADC_CALLBACK_READ_BUFFER);
}

void ADC_Init(void)
{
	struct adc_config config_adc;
	adc_get_config_defaults(&config_adc);
	
	config_adc.clock_prescaler = ADC_CLOCK_PRESCALER_DIV8;
	config_adc.resolution      = ADC_RESOLUTION_8BIT;
	config_adc.clock_source = GCLK_GENERATOR_1;
	
	config_adc.positive_input  = ADC_POSITIVE_INPUT_PIN0; //PA02

	adc_init(&adc_instance, ADC, &config_adc);
	//adc_enable(&adc_instance);
	//adc_disable(&adc_instance);
	//
	configure_battery_callback();
	LoadSwitches_Init();
  Battery_Charger_Init();
}
/************************************************************************/
/************************************************************************/
