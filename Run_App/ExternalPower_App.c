/*
 * ExternalPower_App.c
 *
 * Created: 5/15/2016 8:05:18 PM
 *  Author: ODI
 */ 
#include "ExternalPower_App.h"

#include <compiler.h>
#include "port.h"
//#include "Alarm_Messages.h"

static struct port_config config_port_pin;
#define EXT_POWER_PIN		PIN_PB31


/*=======================================================
* | External Power Check
* =======================================================*/

static bool external_power = false;
bool is_external_power(void){
	return external_power;
}

void checkExternalPower(void)
{
	external_power = port_pin_get_input_level(EXT_POWER_PIN);
}

void ExternalPower_Init(void)
{
	port_get_config_defaults(&config_port_pin);
	
	config_port_pin.direction = PORT_PIN_DIR_INPUT;
	config_port_pin.input_pull = PORT_PIN_PULL_NONE;
	port_pin_set_config(EXT_POWER_PIN, &config_port_pin);
}


/*=======================================================
* | External Power Alarm
* =======================================================*/

static bool LostExternalPower = false;

/************************************************************************/
// Send Message if External Power is Lost
// Send Message if External Power is Regained

void ExternalPower_TaskHandler(void)
{
	checkExternalPower();
	
	//power goes out
	if( is_external_power() == false )
	{
		if( LostExternalPower ){
			return;
		}
		
		//send_lost_external_power_message();
//	  AlarmMessages_Queue_ExternalPowerDisconnectedMessage();	
		LostExternalPower = true;
	}
	
	//power came back on
	else if( LostExternalPower )
	{
//	  AlarmMessages_Queue_ExternalPowerReturnedMessage();	
		LostExternalPower = false;
		//send_regained_external_power_message();
	}
}
