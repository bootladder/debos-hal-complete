/*
 * LED_and_Buzzer_App.h
 *
 * Created: 1/20/2016 12:48:29 PM
 *  Author: ODI
 */ 


#ifndef LED_AND_BUZZER_APP_H_
#define LED_AND_BUZZER_APP_H_

#include <compiler.h>

//#define LED_B_PIN PIN_PA14
//#define LED_G_PIN PIN_PA13
//#define LED_R_PIN PIN_PA12

#define LED_B_PIN PIN_PA13
#define LED_G_PIN PIN_PA14
#define LED_R_PIN PIN_PA12
//blinking yellow every 5, should have been blue every 15
//external power removed, did blue every 5 seconds, shold have been red every 15

void LED_Blink_R(uint16_t time_on);
void LED_Blink_G(uint16_t time_on);
void LED_Blink_B(uint16_t time_on);
bool LEDs_Blinking(void);

void LED_test(void);
void LED_Blink_ALL(uint16_t time_on);


void LED_On_G(void);
void LED_On_R(void);
void LED_On_B(void);
void LED_Off_G(void);
void LED_Off_R(void);
void LED_Off_B(void);

void LED_Init(void);

#endif /* LED_AND_BUZZER_APP_H_ */
