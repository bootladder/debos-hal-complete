/*
 * RTC_App.h
 *
 * Created: 12/11/2015 6:33:42 PM
 *  Author: ODI
 */


#ifndef RTC_APP_H_
#define RTC_APP_H_

#include <compiler.h>

#define TIME_SLOTS_FOR_1_HOUR 240
#define TIME_SLOTS_FOR_5_MINS 20

#define NUMBER_OF_SENSOR_TIME_SLOTS 48
#define NUMBER_OF_VALVE_TIME_SLOTS  4  //number of timeslots in a self heartbeat period.
										//also the same number for valve heartbeat period.

uint32_t RTC_GetMyAbsoluteTimeslot(void);

uint8_t get_SensorTimeSlot(void);
uint8_t get_ValveTimeSlot(void);

void clear_SensorHeartbeatTime(void);
bool is_SensorHeartbeatTime(void);
void clear_ValveHeartbeatTime(void);
bool is_ValveHeartbeatTime(void);
bool is_autostatus_time(void);
void clear_autostatus_time(void);


void RTC_NormalHeartbeatMessageSendFlag_Clear(void);
void RTC_CriticalHeartbeatMessageSendFlag_Clear(void);
bool RTC_NormalHeartbeatMessageSendFlag_IsSet(void);
bool RTC_CriticalHeartbeatMessageSendFlag_IsSet(void);

uint32_t RTC_GetAbsoluteTimepos(void);

bool RTC_IsBatteryReadTime(void);
void clear_battery_read_time(void);

void force_set_sensor_heartbeat(void);
void kick_ValveHeartbeatTimeSlot(void);

void RTC_SetTestModeActive(void);
bool RTC_IsTestModeActive(void);
bool RTC_IsTestModeTimeout(void);
void RTC_ClearTestMode(void);

bool RTC_IsBlinkTime(void);
void RTC_ClearBlinkTime(void);

void RTC_Init(void);

#endif /* RTC_APP_H_ */
