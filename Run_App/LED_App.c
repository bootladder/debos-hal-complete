
/*
 * LED_App.c
 *
 * Created: 8/7/2015 5:32:21 PM
 *  Author: ODI
 */ 
#include "LED_App.h"
#include "Variables.h"

#include <stdint.h>
#include <stdbool.h>
#include <samd20_xplained_pro.h>
#include "gclk.h"
#include "port.h"
#include "tc.h"
#include "tc_interrupt.h"
#include "sysTimer.h"

static void tc_callback_to_turnOff_led_ALL_oneShot(
								struct tc_module *const module_inst);
static void tc_callback_to_turnOff_led_B_oneShot(
								struct tc_module *const module_inst);
static void tc_callback_to_turnOff_led_G_oneShot(
								struct tc_module *const module_inst);
static void tc_callback_to_turnOff_led_R_oneShot(
								struct tc_module *const module_inst);								

#define TC_MODULE_LED_A TC2
#define TC_MODULE_LED_B TC3
static struct tc_config config_tc_LED_A;
static struct tc_config config_tc_LED_B;
static struct tc_module tc_instance_LED_A;
static struct tc_module tc_instance_LED_B;

static bool LED_R_Blinking = false;
static bool LED_G_Blinking = false;
static bool LED_B_Blinking = false;

bool LEDs_Blinking(void)
{
	return ( LED_R_Blinking || LED_G_Blinking || LED_B_Blinking ); 
}


static void tc_callback_to_turnOff_led_B_oneShot(
								struct tc_module *const module_inst)
{
	LED_Off(LED_B_PIN);
	LED_B_Blinking = false;
	tc_disable(&tc_instance_LED_A);
}

//time_on in ms and << 256
void LED_Blink_B(uint16_t time_on)
{
	if( LED_B_Blinking || LED_G_Blinking )
		return;
		
  cpu_irq_disable();

	config_tc_LED_A.counter_8_bit.period = time_on * 2;

	tc_init(&tc_instance_LED_A, TC_MODULE_LED_A, &config_tc_LED_A);
	tc_enable(&tc_instance_LED_A);
	tc_register_callback(&tc_instance_LED_A, tc_callback_to_turnOff_led_B_oneShot,
	TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&tc_instance_LED_A, TC_CALLBACK_OVERFLOW);
	
	LED_B_Blinking = true;
	LED_On(LED_B_PIN);

system_interrupt_enable(TC2_IRQn);

  cpu_irq_enable();
}
/*********************************************************************/
static void tc_callback_to_turnOff_led_G_oneShot(
								struct tc_module *const module_inst)
{
	LED_Off(LED_G_PIN);
	LED_G_Blinking = false;
	
	tc_disable(&tc_instance_LED_A);
}

//time_on in ms and << 256
void LED_Blink_G(uint16_t time_on)
{
	if( LED_B_Blinking || LED_G_Blinking )
		return;

  cpu_irq_disable();

	config_tc_LED_A.counter_8_bit.period = time_on * 2;

	tc_init(&tc_instance_LED_A, TC_MODULE_LED_A, &config_tc_LED_A);
	tc_enable(&tc_instance_LED_A);
	tc_register_callback(&tc_instance_LED_A, tc_callback_to_turnOff_led_G_oneShot,
	TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&tc_instance_LED_A, TC_CALLBACK_OVERFLOW);
	
	LED_G_Blinking = true;
	LED_On(LED_G_PIN);

system_interrupt_enable(TC2_IRQn);
  cpu_irq_enable();
}
/*********************************************************************/
static void tc_callback_to_turnOff_led_R_oneShot(
								struct tc_module *const module_inst)
{
	LED_Off(LED_R_PIN);
	LED_R_Blinking = false;

	tc_disable(&tc_instance_LED_B);
}

#include "AppSystemCheckpoint.h"
#include <string.h>
//time_on in ms and << 256
void LED_Blink_R(uint16_t time_on)
{
	if( LED_R_Blinking )
		return;

  cpu_irq_disable();

	config_tc_LED_B.counter_8_bit.period = time_on * 2;


	tc_init(&tc_instance_LED_B, TC_MODULE_LED_B, &config_tc_LED_B);

	tc_enable(&tc_instance_LED_B);

	tc_register_callback(&tc_instance_LED_B, tc_callback_to_turnOff_led_R_oneShot,
	TC_CALLBACK_OVERFLOW);

	tc_enable_callback(&tc_instance_LED_B, TC_CALLBACK_OVERFLOW);
	
	LED_R_Blinking = true;
	LED_On(LED_R_PIN);

  cpu_irq_enable();
system_interrupt_enable(TC3_IRQn);
  uint32_t my32 = NVIC->ICER[0] ;
 uint8_t i1 = system_interrupt_is_enabled(RTC_IRQn);
 uint8_t i2 = system_interrupt_is_enabled(TC0_IRQn);
 uint8_t i3 = system_interrupt_is_enabled(TC1_IRQn);
 uint8_t i4 = system_interrupt_is_enabled(TC2_IRQn);
 uint8_t i5 = system_interrupt_is_enabled(TC3_IRQn);
 uint8_t i6 = system_interrupt_is_enabled(SERCOM0_IRQn);
 uint8_t i7 = system_interrupt_is_enabled(SERCOM4_IRQn);

	memcpy(checkpointmessagebuffer,"blinkR       \n\0",15);
  checkpointmessagebuffer[7] = '0'+i1;
  checkpointmessagebuffer[8] = '0'+i2;
  checkpointmessagebuffer[9] = '0'+i3;
  checkpointmessagebuffer[10] = '0'+i4;
  checkpointmessagebuffer[11] = '0'+i5;
  checkpointmessagebuffer[12] = '0'+i6;
  checkpointmessagebuffer[13] = '0'+i7;
AppSystemCheckpoint(my32, 0x10|0x2|0x8);

}
/*********************************************************************/

/*********************************************************************/

void LED_test(void)
{
	LED_On(LED_R_PIN);
	LED_On(LED_G_PIN);
	LED_On(LED_B_PIN);
}


static void tc_callback_to_turnOff_led_ALL_oneShot(
	struct tc_module *const module_inst)
{
	LED_Off(LED_R_PIN);
	LED_Off(LED_G_PIN);
	LED_Off(LED_B_PIN);
	LED_R_Blinking = false;
	LED_G_Blinking = false;
	LED_B_Blinking = false;

	tc_disable(&tc_instance_LED_B);
}

//time_on in ms and << 256
void LED_Blink_ALL(uint16_t time_on)
{
	config_tc_LED_B.counter_8_bit.period = time_on * 2;

	tc_init(&tc_instance_LED_B, TC_MODULE_LED_B, &config_tc_LED_B);
	tc_enable(&tc_instance_LED_B);
	tc_register_callback(&tc_instance_LED_B, tc_callback_to_turnOff_led_ALL_oneShot,
	TC_CALLBACK_OVERFLOW);
	tc_enable_callback(&tc_instance_LED_B, TC_CALLBACK_OVERFLOW);
	
	LED_R_Blinking = true;
	LED_G_Blinking = true;
	LED_B_Blinking = true;
	LED_On(LED_R_PIN);
	LED_On(LED_G_PIN);
	LED_On(LED_B_PIN);
}

void LED_On_G(void)
{
	LED_On(LED_G_PIN);
}
void LED_On_R(void)
{
	LED_On(LED_R_PIN);
}
void LED_On_B(void)
{
	LED_On(LED_B_PIN);
}
void LED_Off_G(void)
{
  LED_Off(LED_G_PIN);
}
void LED_Off_R(void)
{
  LED_Off(LED_R_PIN);
}
void LED_Off_B(void)
{
  LED_Off(LED_B_PIN);
}

struct port_config config_port_pin;
//TIMER INIT
void LED_Init(void)
{
	tc_get_config_defaults(&config_tc_LED_A);

	config_tc_LED_A.counter_size = TC_COUNTER_SIZE_8BIT;
	config_tc_LED_A.clock_source = GCLK_GENERATOR_1; //32kHz
	config_tc_LED_A.clock_prescaler = TC_CLOCK_PRESCALER_DIV16; //2kHz ->0.5ms
	
	tc_get_config_defaults(&config_tc_LED_B);

	config_tc_LED_B.counter_size = TC_COUNTER_SIZE_8BIT;
	config_tc_LED_B.clock_source = GCLK_GENERATOR_1; //32kHz
	config_tc_LED_B.clock_prescaler = TC_CLOCK_PRESCALER_DIV16; //2kHz ->0.5ms
	
	
	
	port_get_config_defaults(&config_port_pin);
		
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(LED_G_PIN, &config_port_pin);
	port_pin_set_config(LED_R_PIN, &config_port_pin);
	port_pin_set_config(LED_B_PIN, &config_port_pin);

	port_pin_set_output_level(LED_G_PIN, 1);
	port_pin_set_output_level(LED_R_PIN, 1);
	port_pin_set_output_level(LED_B_PIN, 1);
}
