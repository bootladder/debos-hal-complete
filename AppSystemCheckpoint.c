#include "AppSystemCheckpoint.h"
#include "System.h"
#include "my_memcpy.h"

/*PUBLIC*/ 
uint8_t checkpointmessagebuffer[32];

void AppSystemCheckpoint(uint32_t addr, uint8_t flags)
{
       if( mycheckpointpointer ){
          (mycheckpointpointer)(addr,flags,checkpointmessagebuffer);
       }

        return;
}
/***************************************************************/
//Init pointer to prespecified addrses
void System_Checkpoint_Init(void)
{
  //first dreference the address of checkpoint pointer
  uint32_t * addrptr = (uint32_t *)0x1FF0;
  uint32_t addr = *addrptr;

  mycheckpointpointer = 
      (system_checkpoint_pointer_t*) addr;
}
//////////////////////////////
void AppSystemWriteCheckpointBuffer(uint8_t * buf, uint8_t len)
{
	my_memcpy(checkpointmessagebuffer,buf,len);
}




               //uint8_t flags = 0x2|0x10|0x8;
               //memcpy(buf,"Init YEEEEESS!\n\0",sizeof("Init YEEEEESS!\n\0"));
               //
               //check:
               //(mycheckpointpointer)((uint32_t)(uintptr_t)&&check,flags,buf);
