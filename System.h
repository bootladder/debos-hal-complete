#include <stdint.h>

typedef struct ApplicationHeader{

  void * application_start;
  unsigned int length;
  unsigned int checksum;

} ApplicationHeader_t;

int System_Checkpoint(uint32_t addr, uint8_t flags, uint8_t * msg);

int System_DetectApplication(void);
int System_SetApplicationHeaderAddress(void* addr);
int RunApplication(void);

int System_RunSystem(void);


extern int System_Active;
//put a pointer for the applcatio
#define APP_FUNC_SECTION __attribute__ ((section(".checkpoint_pointer")))
 
#define SYSTEM_CHECKPOINT_ADDRESS 0xFF0
//__attribute__ ((used)) 

typedef int (system_checkpoint_pointer_t)(uint32_t addr, uint8_t flags, uint8_t * msg);




extern system_checkpoint_pointer_t * mycheckpointpointer;
